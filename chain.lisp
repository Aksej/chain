;;;; chain.lisp

(in-package #:chain)

(defmacro chop (var list &body body)
  `(bind (((,var &rest ,list)
           ,list))
     ,@body))

(defun expand-chain (argument forms)
  (if forms
      `(bind ((% ,argument))
         (declare (ignorable %))
         ,(chop form forms
            (if (consp form)
                (case (first form)
                  ('lambda
                      (expand-chain `(,form %)
                       forms))
                  (:var
                   (bind (((var form)
                           (rest form)))
                     `(bind ((,var ,(if (and (consp form)
                                             (not (equal (first form)
                                                         'lambda)))
                                        form
                                        `(,form %))))
                        ,(expand-chain var forms))))
                  (t
                   (expand-chain form forms)))
                (expand-chain `(,form %)
                              forms))))
      argument))

(defmacro => (argument &body body)
  (if (and (consp argument)
           (equal (first argument)
                  :var))
      (bind (((var form)
              (rest argument)))
        `(bind ((,var ,form))
           ,(expand-chain var body)))
      (expand-chain argument body)))

(defmacro set=> (argument &body body)
  (bind ((place (if (and (consp argument)
                         (equal (first argument)
                                :var))
                    (third argument)
                    argument))
         ((:values dummies vals new setter getter)
          (get-setf-expansion place)))
    `(bind (,@(mapcar #'list
                      dummies vals)
            (,(first new)
             ,(if (and (consp argument)
                       (equal (first argument)
                              :var))
                  `(bind ((,(second argument)
                           ,getter))
                     (=> ,(second argument)
                       ,@body))
                  `(=> ,getter
                     ,@body))))
       ,setter)))
