;;;; chain.asd

(asdf:defsystem #:chain
  :description "Two chaining/piping macros, one of them `setf`ing its first
 argument"
  :author "Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>"
  :license  "BSD-3"
  :version "0.0.1"
  :serial t
  :depends-on (#:metabang-bind)
  :components ((:file "package")
               (:file "chain")))
